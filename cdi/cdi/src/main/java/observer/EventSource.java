package observer;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

@Named

public class EventSource {
    @Inject
    private Event<String> simpleMessageEvent;

    public void fireEvent(){
        simpleMessageEvent.fire("" + System.currentTimeMillis());
    }


}
