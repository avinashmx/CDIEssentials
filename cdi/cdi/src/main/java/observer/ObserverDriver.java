package observer;

import interceptor.Pojo;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.CDI;

/**
 * Created by aramana on 9/26/2016.
 */
public class ObserverDriver {
    public static void main(String args[]) throws InterruptedException {
        //GO CDI! GO!!
        Weld weld = new Weld();
        final WeldContainer container = weld.initialize();
        CDI.current().select(EventSource.class).stream().forEach(EventSource::fireEvent);
        weld.shutdown();

    }
}
