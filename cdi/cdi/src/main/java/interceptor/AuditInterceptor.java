package interceptor;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by aramana on 9/26/2016.
 */
@Interceptor
@Audit
public class AuditInterceptor implements Serializable{

    @AroundInvoke
    public Object auditStuff(InvocationContext ctx) throws Exception {
        System.out.println("[AUDIT] " + ctx.getMethod().getName() + Arrays.toString(ctx.getParameters()));
        //or logger.info statement
        return ctx.proceed();
    }
}
