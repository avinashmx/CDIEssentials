package interceptor;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 * Created by aramana on 9/26/2016.
 */
@Audit
public class Pojo implements Serializable {


    private double someField = 0d;

    public void setSomeField(double num) {
        this.someField = num;
    }


}
