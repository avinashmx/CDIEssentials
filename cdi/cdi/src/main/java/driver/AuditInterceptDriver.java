package driver;

import interceptor.Pojo;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

import javax.enterprise.inject.Instance;

/**
 * Created by aramana on 9/26/2016.
 */
public class AuditInterceptDriver {
    public static void main(String args[]) throws InterruptedException {
        //GO CDI! GO!!
        Weld weld = new Weld();
        final WeldContainer container = weld.initialize();
        Instance<Object> instance = container.instance();
        Pojo pojo = instance.select(Pojo.class).get();
        pojo.setSomeField(2.23d);
        weld.shutdown();

    }
}
