package com.gent00.dbconnpool;

/**
 * Created by aramana on 11/17/2016.
 */

/**
 * Associates a DatabasePool with a DatabaseConnection
 */
public class PooledDatabaseConnection extends DatabaseConnection {
    private DatabasePool pool;

    public PooledDatabaseConnection(DatabasePool pool) {
        this.pool = pool;
    }

    @Override
    public void close() {
        pool.returnConnection(this);
    }

}
