package com.gent00.dbconnpool;

import javax.enterprise.inject.Produces;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author codev
 * Date: 1/22/2015@3:34 PM
 */
@Singleton
public class MockDatabasePool implements DatabasePool {


    private final BlockingDeque<DatabaseConnection> readyConnections = new LinkedBlockingDeque<DatabaseConnection>(10);
    private final List<DatabaseConnection> usedConnections = new ArrayList<DatabaseConnection>(10);

    //Main lock, don't want to be messing with both free/used queues together
    private final Lock lockAllConnections = new ReentrantLock();
    private final Condition isDepleted = lockAllConnections.newCondition(); //Signaling both 1. We are out of connections, 2. We have just reclaimed a connection

    /**
     * We hope to only ever see this constructor called ONCE
     */
    public MockDatabasePool() {
        System.out.println("NIO thread impl. started");
        lockAllConnections.lock();
        for (int i = 0; i < 10; i++) {
            DatabaseConnection connection = new PooledDatabaseConnection(this);
            connection.setId("Mock" + i);
            readyConnections.push(connection);
            System.out.printf("Thread[%d] Created connection = %s%n", Thread.currentThread().getId(), connection.getId());
        }
        lockAllConnections.unlock();
    }

    @Produces
    public DatabaseConnection getConnection() throws InterruptedException {
        DatabaseConnection connection = null;
        try {
            lockAllConnections.lock();
            if (readyConnections.peek() == null) {
                System.out.printf("Thread[%d] Waiting up to 1 seconds for new connection%n", Thread.currentThread().getId());
                boolean haveConnectionsDepleted = !(isDepleted.await(1, TimeUnit.SECONDS));
                if (haveConnectionsDepleted) {
                    //Get out of here!
                    throw new InterruptedException("Timeout of 1s is reached. Try again when resource available!");
                } else {
                    //Carry on, below it will take it's connection.
                }
            }
            connection = readyConnections.take();
            usedConnections.add(connection);
        } catch (InterruptedException e) {
            throw e;
        } finally {
            lockAllConnections.unlock();
        }


        System.out.printf("Thread[%d] Leased connection = %s%n", Thread.currentThread().getId(), connection.getId());
        return connection;
    }

    public void returnConnection(DatabaseConnection connection) {
        //Lock , reclaim to ready pool.
        lockAllConnections.lock();
        boolean isReturned = usedConnections.remove(connection);
        readyConnections.add(connection);
        isDepleted.signal(); //We have a connection, signal that we're not depleted. Just one! Not all!
        lockAllConnections.unlock();
        if (!isReturned) {
            System.out.println("Could not return " + connection.getId());
        } else {
            System.out.printf("Thread[%d] Returned connection = %s%n", Thread.currentThread().getId(), connection.getId());
        }
    }
}
