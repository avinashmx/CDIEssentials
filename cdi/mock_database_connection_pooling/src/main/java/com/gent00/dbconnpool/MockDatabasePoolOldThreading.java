package com.gent00.dbconnpool;

import javax.enterprise.inject.Produces;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

/**
 * @Author codev
 * Date: 1/22/2015@3:34 PM
 */
@Singleton
public class MockDatabasePoolOldThreading implements DatabasePool {

    BlockingDeque<DatabaseConnection> connectionQ = new LinkedBlockingDeque<DatabaseConnection>(10);
    List<DatabaseConnection> usedConnectionQ = new ArrayList<DatabaseConnection>(10);     //Main lock, don't want to be messing with both free/used together

    private final Object POOL_LOCK = new Object();
    private static final String THREAD_HEADER = "OldThread";

    public MockDatabasePoolOldThreading() {
        System.err.println("LEGACY THREADS!!! WARNING!!!!");
        //This is still unsafe, another thread will wait on this look, and reinitialize all the connections
        synchronized (POOL_LOCK) {
            for (int i = 0; i < 10; i++) {
                DatabaseConnection connection = new PooledDatabaseConnection(this);
                connection.setId("Mock" + i);
                connectionQ.push(connection);
                System.out.printf(THREAD_HEADER + "[%d] Created connection = %s%n", Thread.currentThread().getId(), connection.getId());
            }
        }

    }

    @Override
    @Produces
    @OldThreading
    public DatabaseConnection getConnection() throws InterruptedException {
        DatabaseConnection connection = null;
        synchronized (POOL_LOCK) {

            //Need to spin until we can get a connection
            //Just because we are "notified" doesn't mean we can get a connection...

                /*
                    Consider this. Two threads(T1,T2) are waiting on a connection, parked on POOL_LOCK.wait. When another
                    thread(T3) returns a connection, they will have notified on POOL_LOCK. T1 will go forward
                    calling take() on the free connection queue. T2 will go forward, but block on take()
                    because only ONE connection from T3 was put back into the pool. At this point, there will be a deadlock.
                    T2 will be holding the POOL_LOCK and any subsequent thread will not be able to acquire the POOL_LOCK to
                    return its connection.

                    You can avoid all this by just using notify(), but who does that anymore?
                 */
            System.out.printf(THREAD_HEADER + "[%d] Waiting up to 2 seconds for new connection%n", Thread.currentThread().getId());
            try {
                connection = connectionQ.poll(2, TimeUnit.SECONDS);

                if (connection == null) {
                    throw new InterruptedException("Where is my connection!?");
                }
            } catch (InterruptedException e) {
                throw e;
            } finally {
                POOL_LOCK.notifyAll();
            }
            usedConnectionQ.add(connection);
        }
        System.out.printf(THREAD_HEADER + "[%d] Leased connection = %s%n", Thread.currentThread().getId(), connection.getId());
        return connection;
    }

    @Override
    public void returnConnection(DatabaseConnection connection) {
        boolean isReturned = false;
        synchronized (POOL_LOCK) {
            isReturned = usedConnectionQ.remove(connection);
            connectionQ.add(connection);
            POOL_LOCK.notifyAll();
        }

        if (!isReturned) {
            System.out.println("Could not return " + connection.getId());
        } else {
            System.out.printf(THREAD_HEADER + "[%d] Returned connection = %s%n", Thread.currentThread().getId(), connection.getId());
            ;
        }
    }

}
