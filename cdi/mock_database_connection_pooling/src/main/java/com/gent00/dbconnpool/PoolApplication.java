package com.gent00.dbconnpool;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import java.util.Random;

/**
 * @Author codev
 * Date: 1/22/2015@3:32 PM
 */

public class PoolApplication {
    private static int TCOUNT = 12;

    public static void main(String args[]) throws InterruptedException {
        //GO CDI! GO!!
        Weld weld = new Weld();
        final WeldContainer container = weld.initialize();

        Thread threads[] = new Thread[TCOUNT];
        //Spin some threads all acting on a unique PoolApplication
        for (int x = 0; x < TCOUNT; x++) {
            Runnable r = new Runnable() {

                public void run() {
                    //Instance many{TCOUNT} SomeBeans in their own thread to contend for MockDatabaseConnections
                    Instance<Object> instance = container.instance();
                    try {
                        SomeBean bean = instance.select(SomeBean.class).get(); //This could take awhile because connection is injected via CDI.
                        System.out.printf("Thread[%d] SomeBean hash = %d%n", Thread.currentThread().getId(), bean.hashCode()); //Prove their unique instances
                        bean.doWork();
                    } catch (Exception e) {
                        if (e.getCause() instanceof InterruptedException) {
                            System.err.printf("Thread[%d] Failed to get a connection!%n", Thread.currentThread().getId());
                        }
                    }
                }
            };
            Thread t = new Thread(r);
            t.start();
            threads[x] = t;
        }

        for (Thread thread : threads) {
            thread.join();
        }
        weld.shutdown();

    }

    /**
     * The connection is injected on new SomeBean() called by CDI. However, we close our own connection since there's no container/proxy to do it for us.
     */
    static class SomeBean {
        @Inject
        @OldThreading
        DatabaseConnection connection;

        public void doWork() {
            try {
                Thread.sleep((long) (20000 * new Random(System.nanoTime()).nextDouble()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            connection.close(); //In a normal container, this would be closed by the "container"/proxy.
        }
    }


}
