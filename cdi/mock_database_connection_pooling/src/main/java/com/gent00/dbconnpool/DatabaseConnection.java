package com.gent00.dbconnpool;

import java.util.Random;

/**
 * @Author codev
 * Date: 1/22/2015@3:34 PM
 */
public abstract class DatabaseConnection {

    public DatabaseConnection() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public abstract void close();

    private String id;
}
