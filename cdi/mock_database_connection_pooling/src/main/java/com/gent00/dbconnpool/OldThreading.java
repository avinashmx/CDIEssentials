package com.gent00.dbconnpool;

import javax.inject.Qualifier;
import java.lang.annotation.RetentionPolicy;

/**
 * @Author codev
 * Date: 1/23/2015@12:18 PM
 */
@java.lang.annotation.Documented
@java.lang.annotation.Retention(RetentionPolicy.RUNTIME)
@javax.inject.Qualifier
public @interface OldThreading {
}
