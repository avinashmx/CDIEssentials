package com.gent00.dbconnpool;

import javax.enterprise.inject.Produces;

/**
 * Created by aramana on 11/17/2016.
 */
public interface DatabasePool {
    @Produces
    @OldThreading
    DatabaseConnection getConnection() throws InterruptedException;

    void returnConnection(DatabaseConnection connection);
}
